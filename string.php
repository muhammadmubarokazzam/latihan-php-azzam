<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width-device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Berlatih String PHP</h1>
    <?php
        echo "<h3>Soal No 1</h3>";
        $kalimat1 = "PHP is never old";
        echo "kalimat pertama : ".$kalimat1."<br>";
        echo "Panjang String : " .strlen($kalimat1)."<br>";
        echo "Jumlah kata : ". str_word_count($kalimat1)."<br><br>";

        $first_sentence = "Hello PHP!";
        echo "kalimat pertama : ".$first_sentence."<br>";
        echo "Panjang String : " .strlen($first_sentence)."<br>";
        echo "Jumlah kata : ". str_word_count($first_sentence)."<br><br>";

        $second_sentence = "I'm ready for the challenges";
        echo "kalimat pertama : ".$second_sentence."<br>";
        echo "Panjang String : " .strlen($second_sentence)."<br>";
        echo "Jumlah kata : ". str_word_count($second_sentence)."<br><br>";


        echo "<h3>Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "Kalimat kedua : ". $string2."<br>";
        echo "kata pertama : ".substr($string2,0,1)."<br>";
        echo "kata kedua : ".substr($string2,2,4)."<br>";
        echo "kata ketiga : ".substr($string2,7,3)."<br><br>";


        echo "<h3>Soal No 3</h3>";
        $string3 = "PHP is old but Good!";
        echo "kalimat ketiga : ".$string3."<br>";
        echo "ganti kalimat ketiga : ".str_replace("Good!","awesome",$string3);

    ?>
</body>
</html>
